from setuptools import setup

setup(
    name='quantyca_structured',
    version='0.0.1',
    description='Test Quantyca Structured',
    url='MarcoQuantyca@bitbucket.org/MarcoQuantyca/test_python_package.git',
    author='M.A.',
    author_email='marco.andreoni@quantyca.it',
    license='unlicense',
    packages=setuptools.find_packages(),
    zip_safe=False
)